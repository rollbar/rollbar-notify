#!/bin/bash

source "$(dirname "$0")/common.sh"

enable_debug
extra_args=""
if [[ "${DEBUG}" == "true" ]]; then
  extra_args="--verbose"
fi

# mandatory variables
ROLLBAR_ACCESS_TOKEN=${ROLLBAR_ACCESS_TOKEN:?'ROLLBAR_ACCESS_TOKEN environment variable missing.'}
ROLLBAR_ENVIRONMENT=${ROLLBAR_ENVIRONMENT:?'ROLLBAR_ENVIRONMENT environment variable missing'}

#Derived parameters
LOCAL_USERNAME='Bitbucket Pipelines'
COMMENT="https://bitbucket.org/${BITBUCKET_REPO_OWNER}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}"

output_file="/tmp/pipe-$RANDOM.txt"

run curl -s --request POST \
      --url https://api.rollbar.com/api/1/deploy/ \
      --output $output_file -w "%{http_code}" \
      --form access_token=$ROLLBAR_ACCESS_TOKEN \
      --form environment=$ROLLBAR_ENVIRONMENT \
      --form revision=$BITBUCKET_COMMIT \
      --form local_username="${LOCAL_USERNAME}" \
      --form comment=$COMMENT \


response=$(cat $output_file)
info "HTTP Response: $(echo $response)"

if [[ "${output}" = 2* ]]; then
  rollbar_deploy_id=$(echo $response | jq -r '.data.deploy_id')
  success "Deploy was successfully reported to Rollbar. You can check your deploy here: https://rollbar.com/deploy/$rollbar_deploy_id"
else
  fail "Something failed. Deploy was not reported to Rollbar."
fi
