# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.2.8

- patch: Clean up and improve build configuration

## 0.2.7

- patch: Patch release to verify that semversioner is working

## 0.2.6

- patch: Patch release to verify that semversioner is working 

## 0.2.5

- patch: Bumping patch version to check build process

## 0.2.4

- patch: Bumping patch version to check build process

## 0.2.3

- patch: Update curl call to check for API failure

## 0.2.2

- patch: Improvements to tests, code cleanup

## 0.2.1

- patch: Minor documentation improvements

## 0.2.0

- minor: Move to rollbar docker hub, Add ROLLBAR_ENVIRONMENT param

## 0.1.0

- minor: Initial version

