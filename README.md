# Bitbucket Pipelines Pipe: Rollbar notify

This pipe will notify Rollbar that your project has been deployed.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: rollbar/rollbar-notify:0.2.8
  variables:
    ROLLBAR_ACCESS_TOKEN: "<string>"
    ROLLBAR_ENVIRONMENT: "<string>"

```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| ROLLBAR_ACCESS_TOKEN (*) | A Rollbar project-level access token with `POST_SERVER_ITEM` scope.  Should be stored as a secured variable. |
| ROLLBAR_ENVIRONMENT (*) | The name of the environment to which you are deploying, e.g. `production`,`staging`, `test`. |

_(*) = required variable._

## Details

This pipe will notify Rollbar of a deploy of your project to a particular environment, making it easier to identify if exceptions were introduced by particular code changes.

## Prerequisites

We recommend storing your Rollbar access token as a secured variable in your Bitbucket repository.  See the following example.

## Examples

Example using secured variable `$ROLLBAR_ACCESS_TOKEN`

```yaml
- pipe: rollbar/rollbar-notify:0.2.8
  variables:
    ROLLBAR_ACCESS_TOKEN: $ROLLBAR_ACCESS_TOKEN
    ROLLBAR_ENVIRONMENT: "production"
```

## Support
If you'd like help with this pipe, or you have an issue or feature request, please contact [support@rollbar.com](mailto:support@rollbar.com).

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## License
Copyright (c) 2019 Rollbar, inc.
MIT licensed, see [LICENSE](LICENSE) file.
